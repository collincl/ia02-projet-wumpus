from wumpus2 import WumpusWorld
from random import *

ww = WumpusWorld()

def isKnownWorld() :
    world_knowledge = True
    stop = False
    for m in range(4) :
        if stop :
            break
        for n in range(4) :
            if ww.deduction[m][n] == "?" :
                world_knowledge = False
                stop = True
                break
    return world_knowledge

ww.probe(0, 0)
ww.guess()

print(isKnownWorld())
print(ww.get_knowledge())

while not(isKnownWorld()) :
    # import pdb; pdb.set_trace()
    stop = False
    for i in range(4) :
        if stop :
            break
        for j in range(4) :
            if ww.deduction[i][j] == "Safe" :
                print("probe[{}][{}]".format(i, j))
                ww.probe(i, j)
                print(ww.get_knowledge())
                stop = True
                break
            elif i == 3 and j == 3 :
                x = randint(0, 3)
                y = randint(0, 3)
                while ww.deduction[x][y] != "?" : # choix aléatoire
                    x = randint(0, 3)
                    y = randint(0, 3)
                print("cautious_probe[{}][{}]".format(x, y))
                ww.cautious_probe(x, y)
                print(ww.get_knowledge())
                stop = True
                break

    ww.guess()

print("Coût exploration : {}".format(ww.cost))
