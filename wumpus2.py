from typing import Dict, Tuple, List

from gopherpysat import Gophersat
gophersat_exec = "/Users/clementcollin/Desktop/UTC/GI02/IA02/gophersat"

__author__ = "Sylvain Lagrue"
__copyright__ = "Copyright 2019, UTC"
__license__ = "LGPL-3.0"
__version__ = "0.3"
__maintainer__ = "Sylvain Lagrue"
__email__ = "sylvain.lagrue@utc.fr"
__status__ = "dev"


class WumpusWorld:
    def __init__(self):
        self.__knowledge = [[False] * 4 for i in range(4)]
        self.__world = [
            ["", "", "P", ""],
            ["P", "", "", ""],
            ["W", "G", "P", ""],
            ["", "", "", "P"],
        ]
        self.__another_world = [
            ["", "", "", ""],
            ["P", "", "P", "W"],
            ["", "G", "P", ""],
            ["", "", "", "P"],
        ]
        self.__position = (0, 0)
        self.__dead = False
        self.__gold_found = False
        self.compute_breeze()
        self.compute_stench()
        self.compute_empty()
        self.cost = 0

        ###############################################################

        self.__voc = ["Wumpus", "Pit", "Safe"]
        self.safe = [[False] * 4 for i in range(4)]
        self.deduction = [["?"] * 4 for i in range(4)]
        self.safe[0][1] = True
        self.deduction[0][1] = "Safe"
        self.__gs = [[Gophersat(gophersat_exec, self.__voc), Gophersat(gophersat_exec, self.__voc), Gophersat(gophersat_exec, self.__voc), Gophersat(gophersat_exec, self.__voc)] for i in range(4)]
        self.compute_gs()

    def compute_gs(self):
        for i in range(4):
            for j in range(4):
                self.__gs[i][j].push_pretty_clause(["Wumpus", "Pit", "Safe"])
                self.__gs[i][j].push_pretty_clause(["-Wumpus", "-Pit"])
                self.__gs[i][j].push_pretty_clause(["-Pit", "-Safe"])
                self.__gs[i][j].push_pretty_clause(["-Wumpus", "-Safe"])

        ###############################################################

    def compute_empty(self):
        for i in range(4):
            for j in range(4):
                if self.__world[i][j] == "":
                    self.__world[i][j] = "."

    def compute_breeze(self):
        for i in range(4):
            for j in range(4):
                if "P" in self.__world[i][j]:
                    if i + 1 < 4 and "B" not in self.__world[i + 1][j]:
                        self.__world[i + 1][j] += "B"
                    if j + 1 < 4 and "B" not in self.__world[i][j + 1]:
                        self.__world[i][j + 1] += "B"
                    if i - 1 >= 0 and "B" not in self.__world[i - 1][j]:
                        self.__world[i - 1][j] += "B"
                    if j - 1 >= 0 and "B" not in self.__world[i][j - 1]:
                        self.__world[i][j - 1] += "B"

    def compute_stench(self):
        for i in range(4):
            for j in range(4):
                if "W" in self.__world[i][j]:
                    if i + 1 < 4:
                        self.__world[i + 1][j] += "S"
                    if j + 1 < 4:
                        self.__world[i][j + 1] += "S"
                    if i - 1 >= 0:
                        self.__world[i - 1][j] += "S"
                    if j - 1 >= 0:
                        self.__world[i][j - 1] += "S"

    def get_knowledge(self):
        res = [["?"] * 4 for i in range(4)]

        for i in range(4):
            for j in range(4):
                if self.__knowledge[i][j] :
                    res[i][j] = self.__world[i][j]
                else :
                    res[i][j] = self.deduction[i][j]

        return res

    def get_position(self) -> Tuple[int, int]:
        return self.__position

    def get_percepts(self) -> str:
        i = self.__position[0]
        j = self.__position[1]
        self.__knowledge[i][j] = True

        return (f"[OK] you feel {self.__world[i][j]}", self.__world[i][j])

    def probe(self, i, j) -> str:
        #if i == 0 and j == 0 and self.__gold_found:
        #    return "[OK] you win !!!"

        if (
            #abs(i - self.__position[0]) + abs(j - self.__position[1]) != 1 or
            i < 0
            or j < 0
            or i >= 4
            or j >= 4
        ):
            return "[err] impossible move..."

        self.__position = (i, j)
        content = self.__world[i][j]
        if "W" in content or "P" in content:
            self.__dead = True
            self.cost += 1000
            return "[KO] The wizard catches a glimpse of the unthinkable and turns mad"
        self.safe[i][j] = True
        self.deduction[i][j] = self.__world[i][j]
        self.cost += 10
        a, b = self.get_percepts()

        if self.__world[i][j] == '.' :

            if i > 0 :
                self.__gs[i - 1][j].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i - 1, j))
            if i < 3 :
                self.__gs[i + 1][j].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i + 1, j))
            if j > 0 :
                self.__gs[i][j - 1].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i, j - 1))
            if j < 3 :
                self.__gs[i][j + 1].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i, j + 1))

        elif self.__world[i][j] == 'B' :

            if i > 0 :
                self.__gs[i - 1][j].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i - 1, j))
            if i < 3 :
                self.__gs[i + 1][j].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i + 1, j))
            if j > 0 :
                self.__gs[i][j - 1].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i, j - 1))
            if j < 3 :
                self.__gs[i][j + 1].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i, j + 1))

            if i == 0 :
                if j == 0 :
                    if self.safe[0][1] :
                        self.__gs[1][0].push_pretty_clause(["Pit"])
                    elif self.safe[1][0] :
                        self.__gs[0][1].push_pretty_clause(["Pit"])
                elif j == 3 :
                    if self.safe[0][2] :
                        self.__gs[1][3].push_pretty_clause(["Pit"])
                    elif self.safe[1][3] :
                        self.__gs[0][2].push_pretty_clause(["Pit"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                    elif self.safe[i][j - 1] and self.safe[i + 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                    elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Pit"])
            elif i == 3 :
                if j == 0 :
                    if self.safe[2][0] :
                        self.__gs[3][1].push_pretty_clause(["Pit"])
                    elif self.safe[3][1] :
                        self.__gs[2][0].push_pretty_clause(["Pit"])
                elif j == 3 :
                    if self.safe[2][3] :
                        self.__gs[3][2].push_pretty_clause(["Pit"])
                    elif self.safe[3][2] :
                        self.__gs[2][3].push_pretty_clause(["Pit"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i - 1][j].push_pretty_clause(["Pit"])
                    elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                    elif self.safe[i - 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Pit"])
            elif j == 0 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Pit"])
            elif j == 3 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Pit"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i + 1][j] and self.safe[i][j - 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Pit"])
            else :
                if self.safe[i][j - 1] and self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i - 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i][j - 1] and self.safe[i + 1][j] and self.safe[i - 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Pit"])

        elif self.__world[i][j] == 'S' :

            if i > 0 :
                self.__gs[i - 1][j].push_pretty_clause(["-Pit"])
                # print("push -pit[{}][{}]".format(i - 1, j))
            if i < 3 :
                self.__gs[i + 1][j].push_pretty_clause(["-Pit"])
                # print("push -pit[{}][{}]".format(i + 1, j))
            if j > 0 :
                self.__gs[i][j - 1].push_pretty_clause(["-Pit"])
                # rint("push -pit[{}][{}]".format(i, j - 1))
            if j < 3 :
                self.__gs[i][j + 1].push_pretty_clause(["-Pit"])
                # print("push -pit[{}][{}]".format(i, j + 1))

            if i == 0 :
                if j == 0 :
                    if self.safe[0][1] :
                        self.__gs[1][0].push_pretty_clause(["Wumpus"])
                    elif self.safe[1][0] :
                        self.__gs[0][1].push_pretty_clause(["Wumpus"])
                elif j == 3 :
                    if self.safe[0][2] :
                        self.__gs[1][3].push_pretty_clause(["Wumpus"])
                    elif self.safe[1][3] :
                        self.__gs[0][2].push_pretty_clause(["Wumpus"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                    elif self.safe[i][j - 1] and self.safe[i + 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                    elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])
            elif i == 3 :
                if j == 0 :
                    if self.safe[2][0] :
                        self.__gs[3][1].push_pretty_clause(["Wumpus"])
                    elif self.safe[3][1] :
                        self.__gs[2][0].push_pretty_clause(["Wumpus"])
                elif j == 3 :
                    if self.safe[2][3] :
                        self.__gs[3][2].push_pretty_clause(["Wumpus"])
                    elif self.safe[3][2] :
                        self.__gs[2][3].push_pretty_clause(["Wumpus"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
                    elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                    elif self.safe[i - 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])
            elif j == 0 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
            elif j == 3 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i + 1][j] and self.safe[i][j - 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
            else :
                if self.safe[i][j - 1] and self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j - 1] and self.safe[i + 1][j] and self.safe[i - 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])

        print(a)
        return b

        ###############################################################

    def guess(self):
        for i in range(4):
            for j in range(4):
                if not(self.__knowledge[i][j]) :
                    self.__gs[i][j].push_pretty_clause(["-Safe"])
                    A = self.__gs[i][j].solve()
                    self.__gs[i][j].pop_clause()
                    self.__gs[i][j].push_pretty_clause(["-Wumpus"])
                    B = self.__gs[i][j].solve()
                    # print(self.__gs[i][j].get_pretty_model())
                    self.__gs[i][j].pop_clause()
                    self.__gs[i][j].push_pretty_clause(["-Pit"])
                    C = self.__gs[i][j].solve()
                    # print(self.__gs[i][j].get_pretty_model())
                    self.__gs[i][j].pop_clause()
                    # print("A = {}, B = {}, C = {}".format(A, B, C))
                    if not(A) and B and C :
                         self.safe[i][j] = True
                         self.deduction[i][j] = "Safe"
                    elif A and not(B) and C :
                         self.deduction[i][j] = "W"
                         for m in range(4):
                             for n in range(4):
                                 if not(self.__knowledge[m][n]) :
                                     self.__gs[m][n].push_pretty_clause(["-Wumpus"])
                    elif A and B and not(C) :
                         self.deduction[i][j] = "P"
                elif self.deduction[i][j] == "?" :
                    self.deduction[i][j] = self.__world[i][j]

    # def deduction2(self):
    #     for i in range(4):
    #         for j in range(4):
    #             if not(self.__knowledge[i][j]) :
    #                 print(self.__gs[i][j].dimacs())

        ###############################################################

    def cautious_probe(self, i, j) -> str:
        #if i == 0 and j == 0 and self.__gold_found:
        #    return "[OK] you win !!!"

        if (
            #abs(i - self.__position[0]) + abs(j - self.__position[1]) != 1 or
            i < 0
            or j < 0
            or i >= 4
            or j >= 4
        ):
            return "[err] impossible move..."

        self.__position = (i, j)
        content = self.__world[i][j]
        self.cost += 100
        a, b = self.get_percepts()

        if self.__world[i][j] == '.' :

            if i > 0 :
                self.__gs[i - 1][j].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i - 1, j))
            if i < 3 :
                self.__gs[i + 1][j].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i + 1, j))
            if j > 0 :
                self.__gs[i][j - 1].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i, j - 1))
            if j < 3 :
                self.__gs[i][j + 1].push_pretty_clause(["Safe"])
                # print("push safe[{}][{}]".format(i, j + 1))

        if self.__world[i][j] == 'B' :

            if i > 0 :
                self.__gs[i - 1][j].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i - 1, j))
            if i < 3 :
                self.__gs[i + 1][j].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i + 1, j))
            if j > 0 :
                self.__gs[i][j - 1].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i, j - 1))
            if j < 3 :
                self.__gs[i][j + 1].push_pretty_clause(["-Wumpus"])
                # print("push -wumpus[{}][{}]".format(i, j + 1))

            if i == 0 :
                if j == 0 :
                    if self.safe[0][1] :
                        self.__gs[1][0].push_pretty_clause(["Pit"])
                    elif self.safe[1][0] :
                        self.__gs[0][1].push_pretty_clause(["Pit"])
                elif j == 3 :
                    if self.safe[0][2] :
                        self.__gs[1][3].push_pretty_clause(["Pit"])
                    elif self.safe[1][3] :
                        self.__gs[0][2].push_pretty_clause(["Pit"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                    elif self.safe[i][j - 1] and self.safe[i + 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                    elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Pit"])
            elif i == 3 :
                if j == 0 :
                    if self.safe[2][0] :
                        self.__gs[3][1].push_pretty_clause(["Pit"])
                    elif self.safe[3][1] :
                        self.__gs[2][0].push_pretty_clause(["Pit"])
                elif j == 3 :
                    if self.safe[2][3] :
                        self.__gs[3][2].push_pretty_clause(["Pit"])
                    elif self.safe[3][2] :
                        self.__gs[2][3].push_pretty_clause(["Pit"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i - 1][j].push_pretty_clause(["Pit"])
                    elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                    elif self.safe[i - 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Pit"])
            elif j == 0 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Pit"])
            elif j == 3 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Pit"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i + 1][j] and self.safe[i][j - 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Pit"])
            else :
                if self.safe[i][j - 1] and self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i - 1][j].push_pretty_clause(["Pit"])
                elif self.safe[i][j - 1] and self.safe[i + 1][j] and self.safe[i - 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Pit"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Pit"])

        elif self.__world[i][j] == 'S' :

            if i > 0 :
                self.__gs[i - 1][j].push_pretty_clause(["-Pit"])
                # print("push -pit[{}][{}]".format(i - 1, j))
            if i < 3 :
                self.__gs[i + 1][j].push_pretty_clause(["-Pit"])
                # print("push -pit[{}][{}]".format(i + 1, j))
            if j > 0 :
                self.__gs[i][j - 1].push_pretty_clause(["-Pit"])
                # rint("push -pit[{}][{}]".format(i, j - 1))
            if j < 3 :
                self.__gs[i][j + 1].push_pretty_clause(["-Pit"])
                # print("push -pit[{}][{}]".format(i, j + 1))

            if i == 0 :
                if j == 0 :
                    if self.safe[0][1] :
                        self.__gs[1][0].push_pretty_clause(["Wumpus"])
                    elif self.safe[1][0] :
                        self.__gs[0][1].push_pretty_clause(["Wumpus"])
                elif j == 3 :
                    if self.safe[0][2] :
                        self.__gs[1][3].push_pretty_clause(["Wumpus"])
                    elif self.safe[1][3] :
                        self.__gs[0][2].push_pretty_clause(["Wumpus"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                    elif self.safe[i][j - 1] and self.safe[i + 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                    elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])
            elif i == 3 :
                if j == 0 :
                    if self.safe[2][0] :
                        self.__gs[3][1].push_pretty_clause(["Wumpus"])
                    elif self.safe[3][1] :
                        self.__gs[2][0].push_pretty_clause(["Wumpus"])
                elif j == 3 :
                    if self.safe[2][3] :
                        self.__gs[3][2].push_pretty_clause(["Wumpus"])
                    elif self.safe[3][2] :
                        self.__gs[2][3].push_pretty_clause(["Wumpus"])
                else :
                    if self.safe[i][j - 1] and self.safe[i][j + 1] :
                        self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
                    elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                        self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                    elif self.safe[i - 1][j] and self.safe[i][j + 1] :
                        self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])
            elif j == 0 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i + 1][j] and self.safe[i][j + 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
            elif j == 3 :
                if self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i + 1][j] and self.safe[i][j - 1] :
                    self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
            else :
                if self.safe[i][j - 1] and self.safe[i][j + 1] and self.safe[i - 1][j] :
                    self.__gs[i + 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j - 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i - 1][j].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j - 1] and self.safe[i + 1][j] and self.safe[i - 1][j] :
                    self.__gs[i][j + 1].push_pretty_clause(["Wumpus"])
                elif self.safe[i][j + 1] and self.safe[i - 1][j] and self.safe[i + 1][j] :
                    self.__gs[i][j - 1].push_pretty_clause(["Wumpus"])

        print(a)
        return b

    def __str__(self) -> str:
        s = ""
        for i in range(4):
            s += f"{i}:"
            for j in range(4):
                s += f" {self.__world[i][j]} "
            s += "\n"

        return s


if __name__ == "__main__":
    ww = WumpusWorld()
    print(ww.get_percepts())
    print(ww.go_to(0, 1))
    print(ww.get_percepts())
    print(ww.get_position())
    print(ww.go_to(0, 2))

    print(ww)
